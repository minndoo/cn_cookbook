import React, { Component } from 'react';
import { Input, FormText } from 'reactstrap';

class DirectionsEdit extends Component {
  render() {
    const { handleData, updateData } = this.props;
    return (
      <div>
        <h5>Directions</h5>
        <hr />
        <Input
          height={500}
          type="textarea"
          name="directions"
          id="exampleText"
          value={updateData.directions}
          onChange={e => {
            handleData(e.target.name, e.target.value);
          }}
        />
        <FormText color="muted">
          <span className="mr-2">
            <strong>*bold*</strong>
          </span>{' '}
          <span className="mr-2">
            <i>_italic_</i>
          </span>{' '}
          <span>*) list</span>
        </FormText>
      </div>
    );
  }
}

export default DirectionsEdit;
