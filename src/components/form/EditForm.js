import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Input } from 'reactstrap';

import EditIngredients from './EditIngredients';
import AddIngredient from './AddIngredient';
import BasicInfo from './BasicInfo';
import PostData from './PostData';
import DirectionsEdit from './DirectionsEdit';
import AddIngredientGroup from './AddIngredientGroup';

class EditForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updateData: {
        ingredients: [],
      },
    };
    this.handleData = this.handleData.bind(this);
  }

  handleData = (field, value) => {
    const { updateData } = this.state;
    this.setState({
      updateData: {
        ...updateData, //spreader function -> dynamic field set
        [field]: value,
      },
    });
  };

  componentDidMount() {
    const { detail_id } = this.props;
    if (detail_id !== 'newRecipe') {
      fetch('https://cookbook.jakubricar.cz/api/recipes/' + detail_id)
        .then(response => response.json())
        .then(resData => {
          this.setState({
            loading: false,
            updateData: resData,
          });
        });
    }
  }

  render() {
    const { detail_id, setPage } = this.props;
    const { updateData } = this.state;
    return (
      <div className="text-left">
        <Row>
          <Col sm="12" className="mt-2">
            {/* header that is dynamically bound with recipe title changes with the value of the recipe title input */}
            <h2 className="float-left">
              {updateData.title
                ? updateData.title
                : detail_id === 'newRecipe'
                  ? 'New Recipe'
                  : 'Recipe Title'}
            </h2>
            {/* PostData component sends POST request and data to the server */}
            <PostData
              detail_id={detail_id}
              setPage={setPage}
              updateData={updateData}
            />
          </Col>
        </Row>
        <hr className="mt-2" />
        <Form>
          <FormGroup>
            {/* this is the input of recipe title */}
            <Input
              type="text"
              name="title"
              defaultValue={updateData.title}
              placeholder="Recipe title"
              onChange={e => {
                this.handleData(e.target.name, e.target.value);
              }}
            />
          </FormGroup>
          <Row className="mt-5">
            <Col sm="2">
              {/* BasicInfo component manages input basic information about
              recipe(servings, preparation time, side dish). */}
              <BasicInfo updateData={updateData} handleData={this.handleData} />
            </Col>
            <Col sm="4">
              <h5>Ingredients</h5>
              <hr />
              {/* EditIngredients component manages the order of ingredients/groups or deletes them */}
              <EditIngredients
                updateData={updateData}
                handleData={this.handleData}
              />
              <div className="mb-3" />
              {/* AddIngredient can add to the ingredient list new ingredient item. */}
              <AddIngredient
                updateData={updateData}
                handleData={this.handleData}
              />
              <AddIngredientGroup
                updateData={updateData}
                handleData={this.handleData}
              />
            </Col>
            <Col sm="6">
              {/* this input manages Directions text area input */}
              <DirectionsEdit
                updateData={updateData}
                handleData={this.handleData}
              />
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default EditForm;
