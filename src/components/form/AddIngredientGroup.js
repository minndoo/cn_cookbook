import React, { Component } from 'react';
import {
  Row,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Button,
} from 'reactstrap';

class AddIngredient extends Component {
  newArr = [];
  constructor(props) {
    super(props);
    this.state = {
      item: {
        name: '',
        isGroup: true,
      },
      canAdd: false,
    };
    this.appendNewIngredient = this.appendNewIngredient.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.updateData !== this.props.updateData) {
      this.newArr = nextProps.updateData.ingredients;
    }
  }

  appendNewIngredient = () => {
    const { item } = this.state;
    const { handleData } = this.props;
    this.newArr.push(item);
    handleData('ingredients', this.newArr);
    this.setState({
      item: {
        ...item,
        name: '',
      },
      canAdd: false,
    });
  };
  handleInput = e => {
    const { item } = this.state;
    this.setState({
      item: {
        ...item,
        name: e.target.value,
      },
    });
    //using this if statement to disable or enable Add button
    //getting name from state doesnt work as intended for an unknown reason
    this.controlAddButton(e.target.value);
  };

  controlAddButton = value => {
    if (value === '') {
      this.setState({
        canAdd: false,
      });
    } else {
      this.setState({
        canAdd: true,
      });
    }
  };

  render() {
    const { name, canAdd } = this.state;
    return (
      <Row className="mt-3">
        <Col>
          <FormGroup>
            <InputGroup>
              <Input
                onChange={this.handleInput}
                type="text"
                name="name"
                value={name}
                placeholder="Name"
              />
              <InputGroupAddon addonType="append">
                <Button
                  disabled={canAdd ? false : true}
                  onClick={this.appendNewIngredient}
                >
                  Add
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
        </Col>
      </Row>
    );
  }
}

export default AddIngredient;
