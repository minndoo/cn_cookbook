import React, { Component } from 'react';
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove,
} from 'react-sortable-hoc';
import { Row, Col, ListGroup, ListGroupItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const RenderedItem = ({ item }) => {
  if (item.isGroup) {
    return (
      <Row>
        <Col sm="12" className="text-center">
          <strong>{item.name}</strong>
        </Col>
      </Row>
    );
  }
  return (
    <Row>
      <Col sm="6">{item.amount + ' ' + item.amountUnit}</Col>
      <Col sm="6">{item.name}</Col>
    </Row>
  );
};

const DragHandle = SortableHandle(() => (
  <span>
    <FontAwesomeIcon icon="bars" />
  </span>
)); // This can be any component you want

const SortableItem = SortableElement(({ item, number, removeItem }) => (
  <ListGroupItem>
    <Row>
      <Col sm="2" className="text-primary">
        <DragHandle />
      </Col>
      <Col sm="8">
        <RenderedItem item={item} />
      </Col>
      <Col sm="2">
        <div
          className="btn btn-link align-middle px-0 py-0"
          onClick={() => {
            removeItem(number);
          }}
        >
          <FontAwesomeIcon icon="times" className="text-danger" />
        </div>
      </Col>
    </Row>
  </ListGroupItem>
));

const SortableList = SortableContainer(({ items, removeItem }) => {
  return (
    <ListGroup>
      {items.map((item, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          item={item}
          number={index}
          removeItem={removeItem}
        />
      ))}
    </ListGroup>
  );
});

class EditIngredients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingredients: [],
    };
    this.removeItem = this.removeItem.bind(this);
  }

  componentDidMount() {
    const { ingredients } = this.props.updateData;
    this.setState({ ingredients: ingredients });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.updateData !== this.props.updateData) {
      this.setState({ ingredients: nextProps.updateData.ingredients });
    }
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { handleData } = this.props;
    this.setState({
      ingredients: arrayMove(this.state.ingredients, oldIndex, newIndex),
    });
    handleData('ingredients', this.state.ingredients);
  };
  removeItem = index => {
    console.log(index);
    this.state.ingredients.splice(index, 1);
    this.props.handleData('ingredients', this.state.ingredients);
  };

  render() {
    const { ingredients } = this.state;

    return (
      <div>
        {ingredients && (
          <SortableList
            lockAxis="y"
            useDragHandle={true}
            items={ingredients}
            onSortEnd={this.onSortEnd}
            removeItem={this.removeItem}
          />
        )}
      </div>
    );
  }
}

export default EditIngredients;
