import React, { Component } from 'react';
import {
  Row,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Button,
  Card,
  CardBody,
  CardHeader,
} from 'reactstrap';

class AddIngredient extends Component {
  newArr = [];
  constructor(props) {
    super(props);
    this.state = {
      item: {
        name: '',
        amount: '',
        amountUnit: '',
      },
      canAdd: false,
    };
    this.appendNewIngredient = this.appendNewIngredient.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.updateData !== this.props.updateData) {
      this.newArr = nextProps.updateData.ingredients;
    }
  }
  appendNewIngredient = () => {
    const { item } = this.state;
    const { handleData } = this.props;
    this.newArr.push(item);
    handleData('ingredients', this.newArr);
    this.setState({
      item: {
        name: '',
        amount: '',
        amountUnit: '',
      },
      canAdd: false,
    });
  };
  handleInput = e => {
    this.setState({
      item: {
        ...this.state.item,
        [e.target.name]: e.target.value,
      },
    });
    //using this if statement to disable or enable Add button
    //getting name from state doesnt work as intended for an unknown reason
    if (e.target.name === 'name') {
      this.controlAddButton(e.target.value);
    }
  };

  controlAddButton = value => {
    if (value === '') {
      this.setState({
        canAdd: false,
      });
    } else {
      this.setState({
        canAdd: true,
      });
    }
  };

  render() {
    const { item, canAdd } = this.state;
    return (
      <Card>
        <CardHeader>Add ingredient</CardHeader>
        <CardBody>
          <Row>
            <Col>
              <FormGroup>
                <Input
                  onChange={this.handleInput}
                  type="number"
                  name="amount"
                  value={item.amount}
                  placeholder="Amount"
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Input
                  onChange={this.handleInput}
                  type="text"
                  name="amountUnit"
                  value={item.amountUnit}
                  placeholder="Unit"
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <InputGroup>
                  <Input
                    onChange={this.handleInput}
                    type="text"
                    name="name"
                    value={item.name}
                    placeholder="Name"
                  />
                  <InputGroupAddon addonType="append">
                    <Button
                      disabled={canAdd ? false : true}
                      onClick={this.appendNewIngredient}
                    >
                      Add
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

export default AddIngredient;
