import React, { Component } from 'react';
import {
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';

class BasicInfo extends Component {
  onChangeHandler = e => {
    const { handleData } = this.props;
    handleData(e.target.name, e.target.value);
  };
  render() {
    const { updateData } = this.props;
    return (
      <div>
        <h5>Basic info</h5>
        <hr />
        {/* preparationTime input control */}
        <FormGroup>
          <Label>
            <small>
              <strong>Preparation Time</strong>
            </small>
          </Label>
          <InputGroup>
            <Input
              onChange={this.onChangeHandler}
              type="number"
              name="preparationTime"
              defaultValue={updateData.preparationTime}
            />
            <InputGroupAddon addonType="append">min</InputGroupAddon>
          </InputGroup>
        </FormGroup>
        {/* servings input control */}
        <FormGroup>
          <Label>
            <small>
              <strong>Servings</strong>
            </small>
          </Label>
          <Input
            onChange={this.onChangeHandler}
            type="number"
            name="servingCount"
            defaultValue={updateData.servingCount}
          />
        </FormGroup>
        {/* side dish input control */}
        <FormGroup>
          <Label>
            <small>
              <strong>Side dish</strong>
            </small>
          </Label>
          <Input
            onChange={this.onChangeHandler}
            type="text"
            name="sideDish"
            defaultValue={updateData.sideDish}
          />
        </FormGroup>
      </div>
    );
  }
}

export default BasicInfo;
