import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PostData extends Component {
  postData = (url, data) => {
    return fetch(url, {
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json',
      },
      method: 'POST',
    }).then(response => response.json());
  };

  sentData = () => {
    const { detail_id, setPage } = this.props;
    let url = `https://cookbook.jakubricar.cz/api/recipes/${
      detail_id === 'newRecipe' ? '' : detail_id
    }`;
    this.postData(url, this.props.updateData).then(resData => {
      setPage('detail', resData._id);
    });
  };
  render() {
    return (
      <Button onClick={this.sentData} className="float-right" color="success">
        <FontAwesomeIcon className="mr-2" icon="save" />
        <small>Save</small>
      </Button>
    );
  }
}

export default PostData;
