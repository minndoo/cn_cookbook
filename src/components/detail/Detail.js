import React, { Component } from 'react';
import RecipeDetail from './RecipeDetail';
import RecipeHeader from './RecipeHeader';

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: false,
      loading: true,
      error: false,
    };
  }

  componentDidMount = () => {
    const { detail_id } = this.props;
    fetch('https://cookbook.jakubricar.cz/api/recipes/' + detail_id)
      .then(response => response.json())
      .then(resData => {
        this.setState({
          loading: false,
          data: resData,
        });
      })
      .catch(error => {
        this.setState({
          error: true,
          loading: false,
        });
      });
  };

  render() {
    const { backToListing, setPage } = this.props;
    const { error, loading, data, isEditing } = this.state;
    return (
      <div>
        {loading && <div>Loading...</div>}
        {error && <div>Something bad hapenned</div>}
        {data && (
          <div>
            <RecipeHeader
              isEditing={isEditing}
              recipeDetail={data}
              backToListing={backToListing}
              setPage={setPage}
            />
            <hr />
          </div>
        )}
        {data &&
          (!isEditing && (
            <RecipeDetail recipeDetail={data} backToListing={backToListing} />
          ))}
      </div>
    );
  }
}

export default Detail;
