import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';

class Directions extends Component {
  /*reformatDirections function rerenders input text 
	from recipe directions into markdown only for specific elements*/

  reformatDirections = sourceText => {
    let newDirections = '';
    let tmpContainer;
    if (sourceText.search('\n\n')) {
      sourceText = sourceText.split('\n\n');
    }
    for (let i = 0; i < sourceText.length; i++) {
      tmpContainer = sourceText[i].split('*)');
      newDirections += tmpContainer[0] + '\n';
      for (let j = 1; j < tmpContainer.length; j++) {
        newDirections += `${j}.` + tmpContainer[j];
      }
    }

    newDirections = newDirections.replace(/\*/g, '**');

    return newDirections;
  };
  render() {
    const { recipeDetail } = this.props;
    let newDirections = '';
    if (recipeDetail.directions) {
      newDirections = this.reformatDirections(recipeDetail.directions);
    }
    return (
      <div>
        <h2>Directions</h2>
        <ReactMarkdown className="text-left" source={newDirections} />
      </div>
    );
  }
}

export default Directions;
