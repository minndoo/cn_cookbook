import React, { Component } from 'react';
import { Table } from 'reactstrap';
import './Ingredients.css';

class Ingredients extends Component {
  render() {
    const { ingredients } = this.props;
    return (
      <div className="border rounded">
        <Table borderless hover className="mb-0">
          <tbody className="text-left tbody-parent">
            {ingredients.map((item, index) => {
              if (item.isGroup) {
                return (
                  <tr key={index} className="border-top">
                    <td colSpan="2" className="text-center">
                      <strong>{item.name}</strong>
                    </td>
                  </tr>
                );
              }
              return (
                <tr key={index} className="border-top">
                  <td className="px-2 py-2">{item.name}</td>
                  <td className="px-2 py-2">{item.amount + item.amountUnit}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Ingredients;
