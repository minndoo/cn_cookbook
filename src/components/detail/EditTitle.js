import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Recipe.css';

class EditTitle extends Component {
  onChangeHandler = event => {
    const { updateData } = this.props;
    updateData('title', event.target.value);
  };
  render() {
    const { title, isEditing, onClickHandler } = this.props;
    if (isEditing) {
      return (
        <div className="align-left ml">
          <input
            type="text"
            defaultValue={title}
            onChange={this.onChangeHandler}
          />
        </div>
      );
    }
    return (
      <div>
        <div className="text-left">
          <Button outline className="align-middle" onClick={onClickHandler}>
            <FontAwesomeIcon icon="arrow-left" />
          </Button>
          <span className="align-left align-middle ml h1">{title}</span>
        </div>
      </div>
    );
  }
}

export default EditTitle;
