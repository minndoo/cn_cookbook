import React, { Component } from 'react';
import { Row, Col, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class EditButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }
  render() {
    const { setPage, sentData, recipeDetail } = this.props;
    return (
      <div>
        <Row>
          <Col sm="8" />
          <Col sm="2">
            <Button
              color="primary"
              onClick={() => {
                setPage('create', recipeDetail._id);
              }}
            >
              <FontAwesomeIcon icon="edit" />
            </Button>
          </Col>
          <Col sm="2">
            <Button onClick={this.toggle} color="danger">
              <FontAwesomeIcon icon="times" />
            </Button>
          </Col>
        </Row>
        <div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
          >
            <ModalBody>
              Do you really wish to delete a tasty recipe of{' '}
              <strong>{recipeDetail.title}</strong>?
            </ModalBody>
            <ModalFooter>
              <Button color="danger" onClick={sentData}>
                Delete
              </Button>{' '}
              <Button color="secondary" onClick={this.toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}

export default EditButtons;
