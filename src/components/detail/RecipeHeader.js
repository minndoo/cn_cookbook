import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EditButtons from './EditButtons';

class RecipeHeader extends Component {
  deleteData = url => {
    return fetch(url, {
      headers: {
        'content-type': 'application/json',
      },
      method: 'DELETE',
    });
  };

  sentData = () => {
    const { backToListing, recipeDetail } = this.props;
    const { _id } = recipeDetail;
    let url = `https://cookbook.jakubricar.cz/api/recipes/${_id}`;
    this.deleteData(url).then(resData => backToListing());
  };
  render() {
    const { isEditing, recipeDetail, backToListing, setPage } = this.props;
    return (
      <Row className="mt-2">
        <Col sm="9" className="text-left">
          {/* go back button, this button reroutes to the recipe list from recipe
          detail */}
          <Button
            color="link"
            className="align-middle text-dark"
            onClick={backToListing}
          >
            <FontAwesomeIcon icon="arrow-left" />
          </Button>
          <span className="h2 text-left align-middle">
            {recipeDetail.title}
          </span>
        </Col>
        <Col sm="3">
          <div>
            {/* Edit buttons will take care of changing the Editing state and also sending POST or DELETE requests */}
            <EditButtons
              sentData={this.sentData}
              isEditing={isEditing}
              recipeDetail={recipeDetail}
              setPage={setPage}
            />
          </div>
        </Col>
      </Row>
    );
  }
}

export default RecipeHeader;
