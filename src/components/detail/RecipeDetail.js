import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './RecipeDetail.css';

import Ingredients from './Ingredients';
import Directions from './Directions';

class RecipeDetail extends Component {
  render() {
    const { recipeDetail } = this.props;
    return (
      <div>
        {recipeDetail.preparationTime && (
          <Row>
            <Col className="text-left">
              <div>
                <FontAwesomeIcon icon="clock" className="mr-2" />
                {`${recipeDetail.preparationTime} min`}
              </div>
            </Col>
          </Row>
        )}
        <Row className="mt-2">
          <Col sm="3">
            {recipeDetail.ingredients && (
              <div>
                <h3 className="align-left">Ingredients</h3>
                {(recipeDetail.servingCount && (
                  <div className="align-left">
                    serving count {recipeDetail.servingCount}
                  </div>
                )) ||
                  ''}
                <Ingredients ingredients={recipeDetail.ingredients} />
              </div>
            )}
          </Col>
          <Col sm="9">
            <Directions recipeDetail={recipeDetail} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default RecipeDetail;
