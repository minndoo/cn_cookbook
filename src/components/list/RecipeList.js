import React, { Component } from 'react';
import { CardColumns } from 'reactstrap';
import RecipeListItem from './RecipeListItem';

class RecipeList extends Component {
  render() {
    const { data, openDetail } = this.props;
    return (
      <CardColumns>
        {data.map((item, index) => {
          return (
            <RecipeListItem openDetail={openDetail} key={index} recipe={item}>
              {item.title}
            </RecipeListItem>
          );
        })}
      </CardColumns>
    );
  }
}

export default RecipeList;
