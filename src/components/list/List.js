import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import RecipeList from './RecipeList';
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: false,
      loading: true,
      error: false,
    };
  }

  componentDidMount = () => {
    this.getRecipeList();
  };

  getRecipeList = () => {
    fetch('https://cookbook.jakubricar.cz/api/recipes/')
      .then(response => response.json())
      .then(resData => {
        this.setState({
          data: resData,
          loading: false,
        });
      })
      .catch(error => {
        console.log('Error', error);
        this.setState({
          error: true,
          loading: false,
        });
      });
  };

  render() {
    const { openPage } = this.props;
    const { data, error, loading } = this.state;
    return (
      <div>
        <div className="clearfix" style={{ padding: '0.5rem' }}>
          <h2 className="float-left">Recipes</h2>
          <button
            onClick={() => {
              openPage('create', 'newRecipe');
            }}
            className="btn btn-primary float-right"
          >
            <FontAwesomeIcon icon="plus-circle" className="mr-2" />New recipe
          </button>
        </div>
        <hr className="mt-0" />
        {loading && <div>Loading...</div>}
        {error && <div>Something bad happenned</div>}
        {data && <RecipeList data={data} openDetail={openPage} />}
      </div>
    );
  }
}

export default List;
