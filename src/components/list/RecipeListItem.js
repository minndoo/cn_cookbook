import React, { Component } from 'react';
import { CardBody, Card, CardTitle, CardText } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './recipelistitem.css';

class RecipeListItem extends Component {
  render() {
    const { recipe, openDetail } = this.props;
    return (
      <Card
        sm="4"
        className="text-left"
        onClick={() => {
          openDetail('detail', recipe._id);
        }}
      >
        <CardBody className="hoverable">
          <CardTitle className="text-primary">{recipe.title}</CardTitle>
          {recipe.preparationTime && (
            <CardText>
              <FontAwesomeIcon icon="clock" className="mr-2" />
              {recipe.preparationTime}
            </CardText>
          )}
        </CardBody>
      </Card>
    );
  }
}

export default RecipeListItem;
