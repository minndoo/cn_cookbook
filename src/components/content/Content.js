import React, { Component } from 'react';
import { Container, Row, Button } from 'reactstrap';

// import TestingComponent from './components/TestingComponent';
// import AhojComponent from './components/AhojComponent';

import List from '../list/List';
import Detail from '../detail/Detail';
import EditForm from '../form/EditForm';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 'list', // 'list' or 'detail'
      detail_id: false,
    };
  }

  //predesle 2 funkce mergnute do jedne
  setPage = (newPage, detail_id) => {
    this.setState({
      page: newPage,
      detail_id: detail_id,
    });
  };

  render() {
    const { detail_id } = this.state;

    return (
      <div>
        <Row className="mt-3">
          <Button
            className="mx-auto"
            onClick={() => {
              this.setPage('list', false);
            }}
          >
            Cookbook
          </Button>
        </Row>
        <Container>
          {this.state.page === 'list' && <List openPage={this.setPage} />}

          {this.state.page === 'detail' && (
            <Detail
              detail_id={detail_id}
              setPage={this.setPage}
              backToListing={() => {
                this.setPage('list', false);
              }}
            />
          )}
          {this.state.page === 'create' && (
            <EditForm setPage={this.setPage} detail_id={detail_id} />
          )}
        </Container>
      </div>
    );
  }
}

export default Content;
