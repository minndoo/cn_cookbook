import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faClock,
  faPlusCircle,
  faArrowLeft,
  faEdit,
  faTimes,
  faSave,
  faBars,
} from '@fortawesome/free-solid-svg-icons';

// import TestingComponent from './components/TestingComponent';
// import AhojComponent from './components/AhojComponent';

import Content from './components/content/Content';

// FontAwesomeIcons
library.add(
  faClock,
  faPlusCircle,
  faArrowLeft,
  faEdit,
  faTimes,
  faSave,
  faBars,
);

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Our testing app</h1>
        </header>

        <Content />
      </div>
    );
  }
}

export default App;
